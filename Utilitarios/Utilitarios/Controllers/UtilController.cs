﻿using Controlador;
using Modelo.beans;
using Modelo.constantes;
using Newtonsoft.Json;
using NexusUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Utilitarios.Controllers
{
    public class UtilController : ApiController
    {
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public BeanGeneric Encriptar(BeanEncriptar beanEncriptar)
        {
            BeanGeneric beanGeneric = new BeanGeneric();
            try
            {
                Log.RegistrarLog("INPUT ENCRIPTAR: " + beanEncriptar.palabra);
                beanGeneric.idResultado = Resultado.OK_NOMSG;
                beanGeneric.value = Controlador.Encriptar.EncriptarTripleDES(beanEncriptar.palabra);
            }
            catch (Exception ex)
            {
                beanGeneric.idResultado = Resultado.ERROR_NOMSG;
                beanGeneric.value = ex.Message;
                Log.RegistrarLog("ERROR Encriptar: " + ex.Message);
            }

            return beanGeneric;
        }

        [HttpPost]
        [EnableCors(origins:"*",headers:"*",methods:"*")]
        public BeanGeneric Desencriptar(BeanEncriptar beanEncriptar)
        {
            BeanGeneric beanGeneric = new BeanGeneric();
            try
            {
                Log.RegistrarLog("INPUT Desencriptar: " + beanEncriptar.palabra);
                beanGeneric.idResultado = Resultado.OK_NOMSG;
                beanGeneric.value = Controlador.Encriptar.DesencriptarTripleDES(beanEncriptar.palabra);
            }
            catch (Exception ex)
            {
                beanGeneric.idResultado = Resultado.ERROR_NOMSG;
                beanGeneric.value = ex.Message;
                Log.RegistrarLog("ERROR Desencriptar: " + ex.Message);
            }
            return beanGeneric;
        }

        [HttpGet]
        public BeanGeneric EncriptarMasivo(int idEmpresa)
        {

            BeanGeneric respuesta = new BeanGeneric();

            try
            {
                List<BeanCliente> lista = Controlador.Encriptar.ListaCliente(idEmpresa);

              
                for (int i = 0; i < lista.Count; i++)
                {
                    lista[i].claveEncriptada= Controlador.Encriptar.EncriptarTripleDES(lista[i].documentoIdentidad);


                }
                Controlador.Encriptar.ActualizaClaves(lista);
                
            }
            catch (Exception ex)
            {
                respuesta.resultado = ex.Message;
            }

            return respuesta;
        }


    }
}

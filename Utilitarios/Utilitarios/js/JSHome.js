﻿var urlDefault = "Pages/Home.aspx";

function DireccionarMenu() {
    PaginaDefault();
    EventosMenu();
}

function PaginaDefault() {
    $.ajax({
        type: 'GET',
        url: urlDefault,
        contentType: 'application/json; charset=utf-8',
        dataType: 'html',
        async: true,
        cache: false,
        beforeSend: function () {
            $("#containerPage").empty();
            //$("#containerPage").html("<img src='css/Facebook.gif' class='rounded blocktext' alt='Responsive image'>");
        },
        success: function (data) {
            if (data != null) {
                $("#containerPage").empty();
                $("#containerPage").html(data);
            }
        },
        error: function (xhr, status, error) {
            console.log(error);
        }
    });
}

function EventosMenu() {
    $(".nav-item").click(function () {
        $(".nav-item").removeClass('active');
        $(this).addClass('active');
    });

    $(".menuOption").click(function () {
        var url = $(this).attr('url');
        $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json; charset=utf-8',
            dataType: 'html',
            async: true,
            cache: false,
            beforeSend: function () {
                $("#containerPage").empty();
                //$("#containerPage").html("<img src='css/Facebook.gif' class='rounded blocktext' alt='Responsive image'>");
            },
            success: function (data) {
                if (data != null) {
                    $("#containerPage").empty();
                    $("#containerPage").html(data);
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });
    });
}
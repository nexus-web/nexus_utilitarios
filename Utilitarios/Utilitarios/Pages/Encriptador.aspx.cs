﻿using NexusUtil;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Utilitarios.Pages
{
    public partial class Encriptador : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                hdnUrlEncriptar.Value = ConfigurationManager.AppSettings["RUTAAPI"];
                Log.RegistrarLog("RUTA API: " + hdnUrlEncriptar.Value);
            }
            catch (Exception ex)
            {
                Log.RegistrarLog("ERROR Encriptador.aspx: " + ex.Message);
            }
        }
    }
}
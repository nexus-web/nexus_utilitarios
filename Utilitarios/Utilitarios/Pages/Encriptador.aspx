﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Encriptador.aspx.cs" Inherits="Utilitarios.Pages.Encriptador" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <div class="row">
        <div class="col-xs-12 col-md-12- col-lg-12">
            <div class="input-group">
                <div class="input-group-btn">
                    <button id="btnAccion" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Encriptar
                    </button>
                    <div class="dropdown-menu">
                        <a id="btnEncriptar" class="dropdown-item" href="#">Encriptar</a>
                        <a id="btnDesecriptar" class="dropdown-item" href="#">Desencriptar</a>
                    </div>
                </div>
                <input id="txtEncriptado" name="txtEncriptado" type="text" class="form-control" placeholder="Ingresar palabra" aria-label="Text input with dropdown button" />
            </div>

            <%--<select id="cbxAccion" class="form-control col-xs-4 col-md-4">
                <option value="1">Encriptar</option>
                <option value="2">Desencriptar</option>
            </select>

            <div class="form-group">
                <label for="txtEncriptado">Encriptar: </label>
                <input id="txtEncriptado" name="txtEncriptado" type="text" class="form-control" placeholder="Ingresar encriptado" />
            </div>

            <div class="form-group">
                <label for="txtDesencriptar">Desencriptado: </label>
                <input id="txtDesencriptar" name="txtDesencriptar" type="text" class="form-control" readonly="readonly" />
            </div>

            <button id="btnEjecutar" type="button" class="btn btn-primary">Ejecutar</button>--%>
        </div>
    </div>
    <div class="row" style="margin-top:30px;">
        <div class="col-xs-12 col-md-12- col-lg-12">
            <div class="form-group">
                <label for="txtDesencriptar">Resultado: </label>
                <input id="txtDesencriptar" name="txtDesencriptar" type="text" class="form-control" readonly="readonly" />
            </div>
        </div>
    </div>

    <input id="hdnUrlEncriptar" type="hidden" runat="server" />

    <script src="Pages/js/JSEncriptador.js"></script>
    <script>
        $(document).ready(function () {
            CargaInicial();
        })
    </script>
</body>
</html>

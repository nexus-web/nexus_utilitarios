﻿function CargaInicial() {
    Eventos();
}

function Eventos() {
    $('#txtEncriptado').keypress(function (e) {
        if (e.keyCode == 13) {
            var texto = $("#btnAccion").text();
            if (texto.indexOf("D") == -1) {
                $("#btnAccion").text('Encriptar');
                var url = $("#hdnUrlEncriptar").val() + 'Encriptar';
            } else {
                $("#btnAccion").text('Desencriptar');
                var url = $("#hdnUrlEncriptar").val() + 'Desencriptar';
            }

            var strData = new Object();
            strData.palabra = $("#txtEncriptado").val();
            console.log(url);
            if (strData.palabra.length > 0) {
                $.ajax({
                    type: 'POST',
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (rpta) {
                        //var rpta = $.parseJSON(data);
                        if (rpta.idResultado == 1) {
                            $("#txtDesencriptar").val(rpta.value);
                            //alertify.success('Flag Servicio Editado correctamente');
                        }
                        else {

                        }
                    },
                    error: function (xhr, status, error) {
                        //alertify.error(error);
                    }
                });
            } else {
                $("#txtDesencriptar").val('');
            }
        }
    });

    $("#btnEncriptar").click(function () {
        $("#txtDesencriptar").val('');
        $("#btnAccion").text('Encriptar');
        var url = $("#hdnUrlEncriptar").val() + 'Encriptar';
        var strData = new Object();
        strData.palabra = $("#txtEncriptado").val();
        console.log(url);
        if (strData.palabra.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (rpta) {
                    //var rpta = $.parseJSON(data);
                    if (rpta.idResultado == 1) {
                        $("#txtDesencriptar").val(rpta.value);
                        //alertify.success('Flag Servicio Editado correctamente');
                    }
                    else {

                    }
                },
                error: function (xhr, status, error) {
                    //alertify.error(error);
                }
            });
        }
    });

    $("#btnDesecriptar").click(function () {
        $("#txtDesencriptar").val('');
        $("#btnAccion").text('Desencriptar');
        var url = $("#hdnUrlEncriptar").val() + 'Desencriptar';
        var strData = new Object();
        strData.palabra = $("#txtEncriptado").val();
        if (strData.palabra.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (rpta) {
                    //var rpta = $.parseJSON(data);
                    if (rpta.idResultado == 1) {
                        $("#txtDesencriptar").val(rpta.value);
                        //alertify.success('Flag Servicio Editado correctamente');
                    }
                    else {

                    }
                },
                error: function (xhr, status, error) {
                    //alertify.error(error);
                }
            });
        }
    });
}
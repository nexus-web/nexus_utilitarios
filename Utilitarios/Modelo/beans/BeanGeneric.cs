﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.beans
{
    public class BeanGeneric
    {
        public Int32 idResultado { get; set; }
        public String resultado { get; set; }
        public String value { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.beans
{
    public class BeanCliente
    {
        public String documentoIdentidad { get; set; }
        public int idCliente { get; set; }
        public String claveEncriptada { get; set; }

    }
}

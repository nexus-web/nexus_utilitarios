﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilidad;

namespace Modelo
{
    public class MCliente
    {
        public DataTable ListarClientes(int idEmpresa)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            SqlParameter parameter = new SqlParameter("@idEmpresa",SqlDbType.Int);

            parameter.Value = idEmpresa;
            lstParameter.Add(parameter);

            return SqlHelper.ExecuteDataset(SqlConnector.getConnectionString(), CommandType.StoredProcedure, "X18_ListaClienteXEmpresa", lstParameter.ToArray()).Tables[0];

        }

        public void ActualizarClavesClientes(DataTable clientes)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            SqlParameter parameter = new SqlParameter("@DTCLIENTETEMPORAL", SqlDbType.Structured);

            parameter.Value = clientes;
            lstParameter.Add(parameter);

             SqlHelper.ExecuteNonQuery(SqlConnector.getConnectionString(), CommandType.StoredProcedure, "X18_ActualizarClaveMasiva", lstParameter.ToArray());

        }
    }
}

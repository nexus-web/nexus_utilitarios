﻿using Modelo;
using Modelo.beans;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Controlador
{
    public class Encriptar
    {
        private const string iFrasePasswd = "15646^&amp;%$3(),>2134bgGz*-+e7hds";

        public static string EncriptarTripleDES(string cadena)
        {
            byte[] resultados;

            UTF8Encoding utf8 = new UTF8Encoding();
            MD5CryptoServiceProvider provHash = new MD5CryptoServiceProvider();
            byte[] llaveTDES = provHash.ComputeHash(utf8.GetBytes(iFrasePasswd));
            TripleDESCryptoServiceProvider algTDES = new TripleDESCryptoServiceProvider()
            {
                Key = llaveTDES,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            // Obtenemos el array de bytes de nuestra cadena a tratar
            byte[] datoEncriptar = utf8.GetBytes(cadena);
            try
            {
                // Generemos en encriptador para nuestro proceso
                ICryptoTransform encriptador = algTDES.CreateEncryptor();
                resultados = encriptador.TransformFinalBlock(datoEncriptar, 0, datoEncriptar.Length);
            }
            finally
            {
                // Liberemos los recursos
                algTDES.Clear();
                provHash.Clear();
            }
            return Convert.ToBase64String(resultados);
        }

        public static string DesencriptarTripleDES(string cadena)
        {
            byte[] resultados;
            UTF8Encoding utf8 = new UTF8Encoding();
            MD5CryptoServiceProvider provHash = new MD5CryptoServiceProvider();
            byte[] llaveTDES = provHash.ComputeHash(utf8.GetBytes(iFrasePasswd));
            TripleDESCryptoServiceProvider algTDES = new TripleDESCryptoServiceProvider()
            {
                Key = llaveTDES,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            byte[] datoADesencriptar = Convert.FromBase64String(cadena);
            try
            {
                ICryptoTransform desencr = algTDES.CreateDecryptor();
                resultados = desencr.TransformFinalBlock(datoADesencriptar, 0, datoADesencriptar.Length);
            }
            finally
            {
                algTDES.Clear();
                provHash.Clear();
            }

            return utf8.GetString(resultados);
        }

        public static List<BeanCliente> ListaCliente(int idEmpresa)
        {

            BeanCliente beanE = null;
            List<BeanCliente> lstBeanC = new List<BeanCliente>();
            DataTable dt = new MCliente().ListarClientes(idEmpresa);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    beanE = new BeanCliente();
                    beanE.documentoIdentidad = row["DocumentoIdentidad"] == DBNull.Value ? "" : Convert.ToString(row["DocumentoIdentidad"]);
                    beanE.idCliente = row["idCliente"] == DBNull.Value ? 0 : Convert.ToInt32(row["idCliente"]);

                    lstBeanC.Add(beanE);
                }
            }

            return lstBeanC;
        }

        public static void ActualizaClaves(List<BeanCliente> lstBeanClientes)
        {
            DataTable dtCliente = new DataTable();
            
            dtCliente.Columns.Add("id", typeof(System.Int32));
            dtCliente.Columns.Add("usuario", typeof(System.String));
            dtCliente.Columns.Add("totalServicio", typeof(System.Int32));


            foreach (BeanCliente bean in lstBeanClientes)
            {
                dtCliente.Rows.Add(bean.idCliente, bean.claveEncriptada);
            }

            new MCliente().ActualizarClavesClientes(dtCliente);
            

        }

    }
}

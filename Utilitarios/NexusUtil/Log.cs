﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace NexusUtil
{
    public class Log
    {
        public static void RegistrarLog(String msg)
        {
            String LOG_ACTIVO = ConfigurationManager.AppSettings["LOG_ACTIVO"];

            if (LOG_ACTIVO == "1")
            {
                String ruta = HostingEnvironment.ApplicationPhysicalPath + ConfigurationManager.AppSettings["RUTALOG"];

                if (!Directory.Exists(ruta))
                {
                    Directory.CreateDirectory(ruta);
                }

                Write(ruta, msg);
            }
        }

        private static void Write(string rutaCompletaLog, string sMessage)
        {
            StreamWriter streamWriter = new StreamWriter(rutaCompletaLog + DateTime.Now.ToString("MM-dd-yyyy") + ".Log", true, (Encoding)new ASCIIEncoding());
            streamWriter.Write("\r\nLog Entry : ");
            streamWriter.WriteLine("{0}", (object)DateTime.Now.ToString("MM/dd/yyyy HH:mm.ss.ffff"));
            streamWriter.WriteLine("  :");
            streamWriter.WriteLine("  :{0}", (object)sMessage);
            streamWriter.WriteLine("-------------------------------");
            streamWriter.Flush();
            streamWriter.Close();
        }
    }
}
